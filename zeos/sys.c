/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <sched.h>

#define LECTURA 0
#define ESCRIPTURA 1

int check_fd(int fd, int permissions)
{
  if (fd!=1) return -9; /*EBADF*/
  if (permissions!=ESCRIPTURA) return -13; /*EACCES*/
  return 0;
}

int sys_ni_syscall()
{
	return -38; /*ENOSYS*/
}

int sys_getpid()
{
	return current()->PID;
}

int ret_from_fork() {
  stats_ready_to_system();
  return 0;
}

int sys_fork()
{
  int PID;
  int pag;
  
  //Check if we have free pcb
  if (list_empty(&freequeue)) {
    return -156;		
  }

  //Alocate memory for data+stack
  //Done before so we don't have to undo changes
  int frames[NUM_PAG_DATA];
  for (pag = 0; pag < NUM_PAG_DATA; pag++) {
    frames[pag] = alloc_frame();
    if (frames[pag] == -1) { //Not enough memory
      //Free the allocated memory
      for (pag--; pag >= 0; pag--) {
	free_frame(frames[pag]);	    
      }
      return -156;
    }
  }

  //Get new pcb
  struct list_head *t_anchor = list_first(&freequeue);
  list_del(t_anchor);
  struct task_struct *task = list_head_to_task_struct(t_anchor);
  
  //Copy parent's task_union to child's task_union
  copy_data(current(), task, 4096);
  
  //Allocate new directory
  allocate_DIR(task);

  //Get both parent and child page tables
  page_table_entry * process_PT_parent = get_PT(current());
  page_table_entry * process_PT_child = get_PT(task);

  //Map the same code frames of the parent to the child
  for (pag = 0; pag < NUM_PAG_CODE; pag++) {
    set_ss_pag(process_PT_child, PAG_LOG_INIT_CODE+pag, get_frame(process_PT_parent, PAG_LOG_INIT_CODE+pag));
  }

  //Map the new allocated data frames to the child and temporary to the parent
  //(so we can access them from the parent)
  for (pag = 0; pag < NUM_PAG_DATA; pag++) {
    int frame = frames[pag];
    set_ss_pag(process_PT_child, PAG_LOG_INIT_DATA+pag, frame);
    set_ss_pag(process_PT_parent, PAG_LOG_INIT_DATA+NUM_PAG_DATA+pag, frame);
  }

  //Copy data+stack memory contents
  copy_data(PAG_LOG_INIT_DATA*PAGE_SIZE, (PAG_LOG_INIT_DATA+NUM_PAG_DATA)*PAGE_SIZE, NUM_PAG_DATA*PAGE_SIZE);

  //Undo the temporary map of child's frames to parent's pages
  for (pag = 0; pag < NUM_PAG_DATA; pag++) {
    del_ss_pag(process_PT_parent, PAG_LOG_INIT_DATA+NUM_PAG_DATA+pag);
  }

  //Flush the TLB
  set_cr3(current()->dir_pages_baseAddr);

  //Assign PID
  PID = proc_num++;
  task->PID = PID;

  //Assign initial quantum (same as the parent)
  set_quantum(task, get_quantum(current()));

  //Initialise stats struct
  task->process_stats.user_ticks = 0;
  task->process_stats.system_ticks = 0;
  task->process_stats.blocked_ticks = 0;
  task->process_stats.ready_ticks = 0;
  task->process_stats.elapsed_total_ticks = get_ticks();
  task->process_stats.total_trans = 0;
  task->process_stats.remaining_ticks = get_quantum(task);
  

  //Modify the system stack properly (adding ret_from_fork and dummy ebp)
  int * stack_child = (int *)task;
  int * stack_parent = (int *)current();

  int offset = CTX_HW + CTX_SW + 2;
  stack_child[1024-offset] = ret_from_fork;
  stack_child[1024-offset-1] = 0xCAFE;

  //Calculate the top of the system stack of the child
  task->KERNEL_ESP = &stack_child[1024-offset-1];

  //Add the child process to the readyqueue
  list_add_tail(&(task->list), &readyqueue);
  
  //Return the PID to the parent (child return value is set in ret_from_fork)
  return PID;
}

void sys_exit() {
  page_table_entry * process_PT = get_PT(current());

  for (int pag = 0; pag < NUM_PAG_DATA; pag++) {
    free_frame(get_frame(process_PT, PAG_LOG_INIT_DATA+pag));
  }

  current()->PID = 0;
  current()->KERNEL_ESP = 0;
  current()->dir_pages_baseAddr = 0;
  update_process_state_rr(current(), &freequeue);
  current()->initial_quantum = 0;
  
  sched_next_rr();
}

int sys_write(int fd, char *b, int size) {
  int error = check_fd(fd, ESCRIPTURA);
  //max length arbitrary 256 
  char buffer[256];
  copy_from_user(b, buffer, min(size, 256));
  if (error) return error;
  if (buffer == NULL) return -14;
  if (size < 0) return -14;
  return sys_write_console(buffer, min(size, 256));
}

int sys_gettime() {
  return zeos_ticks;
}

int sys_get_stats(int pid, struct stats *st) {
  int i;
  for (i = 0; i < NR_TASKS; ++i) {
    if (task[i].task.PID == pid) break;
  }
  if (i == NR_TASKS) return -3; // ESRCH
  if (st < (PAG_LOG_INIT_DATA << 12)) return -13; // EACCESS
  copy_to_user(&task[i].task.process_stats, st, sizeof(struct stats));
  return 0;
}
