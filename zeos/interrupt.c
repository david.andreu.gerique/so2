/*
 * interrupt.c -
 */
#include <types.h>
#include <interrupt.h>
#include <segment.h>
#include <hardware.h>
#include <io.h>
#include <sched.h>

#include <zeos_interrupt.h>

Gate idt[IDT_ENTRIES];
Register    idtR;

// Distribuci� brit�nica
char char_map[] =
{
  '\0','\0','1' ,'2' ,'3' ,'4' ,'5' ,'6' , '7' ,'8' ,
  '9' ,'0' ,'-' , '=' , 8  ,'\t', 'q' ,'w' ,'e' ,'r' ,
  't' ,'y' ,'u' ,'i' , 'o' ,'p' ,'[' ,']' ,'\n','\0',
  'a' ,'s' ,'d' ,'f' ,'g' ,'h' ,'j' ,'k' ,'l' ,';' ,
  '\'','#' ,'\0','\\','z' ,'x' ,'c' ,'v' , 'b' ,'n' ,
  'm' ,',' ,'.' ,'/' ,'\0','*' , '\0',' ' ,'\0','\0',
  '\0','\0','\0','\0', '\0','\0','\0','\0','\0','\0',
  '\0','7' , '8' ,'9' ,'-' ,'4' ,'5' ,'6' ,'+' ,'1' ,
  '2' ,'3' ,'0' ,'\0','\0','\0','<' ,'\0', '\0','\0',
  '\0','\0','\0','\0','\0','\0', '\0','\0'
};

char mayus_char_map[] =
{
  '\0','\0','!' ,'\@' ,'�' ,'$' ,'%' ,'^' , '&' ,'*',
  '(' ,')' ,'_' , '+' , 8  ,'\t', 'Q' ,'W' ,'E' ,'R' ,
  'T' ,'Y' ,'U' ,'I' , 'O' ,'P' ,'{' ,'}' ,'\n','\0',
  'A' ,'S' , 'D' ,'F' ,'G' ,'H' ,'J' ,'K' ,'L' ,':' ,
  '"','~' ,'\0','|' ,'Z' ,'X' ,'C' ,'V' , 'B' ,'N' ,
  'M' ,'<' ,'>' ,'?' ,'\0','*' , '\0',' ' ,'\0','\0',
  '\0','\0','\0','\0', '\0','\0','\0','\0','\0','\0',
  '\0','7' , '8' ,'9' ,'-' ,'4' ,'5' ,'6' ,'+' ,'1' ,
  '2' ,'3' ,'0' ,'\0','\0','\0','>' ,'\0', '\0','\0',
  '\0','\0','\0','\0','\0','\0', '\0','\0'
};

Byte shifted = 0;

void setInterruptHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
  /***********************************************************************/
  /* THE INTERRUPTION GATE FLAGS:                          R1: pg. 5-11  */
  /* ***************************                                         */
  /* flags = x xx 0x110 000 ?????                                        */
  /*         |  |  |                                                     */
  /*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
  /*         |   \ DPL = Num. higher PL from which it is accessible      */
  /*          \ P = Segment Present bit                                  */
  /***********************************************************************/
  Word flags = (Word)(maxAccessibleFromPL << 13);
  flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

  idt[vector].lowOffset       = lowWord((DWord)handler);
  idt[vector].segmentSelector = __KERNEL_CS;
  idt[vector].flags           = flags;
  idt[vector].highOffset      = highWord((DWord)handler);
}

void setTrapHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
  /***********************************************************************/
  /* THE TRAP GATE FLAGS:                                  R1: pg. 5-11  */
  /* ********************                                                */
  /* flags = x xx 0x111 000 ?????                                        */
  /*         |  |  |                                                     */
  /*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
  /*         |   \ DPL = Num. higher PL from which it is accessible      */
  /*          \ P = Segment Present bit                                  */
  /***********************************************************************/
  Word flags = (Word)(maxAccessibleFromPL << 13);

  //flags |= 0x8F00;    /* P = 1, D = 1, Type = 1111 (Trap Gate) */
  /* Changed to 0x8e00 to convert it to an 'interrupt gate' and so
     the system calls will be thread-safe. */
  flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

  idt[vector].lowOffset       = lowWord((DWord)handler);
  idt[vector].segmentSelector = __KERNEL_CS;
  idt[vector].flags           = flags;
  idt[vector].highOffset      = highWord((DWord)handler);
}


void setIdt()
{
  /* Program interrups/exception service routines */
  idtR.base  = (DWord)idt;
  idtR.limit = IDT_ENTRIES * sizeof(Gate) - 1;
  
  set_handlers();

  /* ADD INITIALIZATION CODE FOR INTERRUPT VECTOR */
  setInterruptHandler(33, keyboard_handler, 0);
  setInterruptHandler(32, clock_handler, 0);
  setTrapHandler(0x80, system_call_handler, 3);

  set_idt_reg(&idtR);
}

int keyboard_routine() {
  Byte value = inb(0x60);
  Byte make = !(value >> 7);
  int scan_code = value & 0x7F;
  char c;
  if (make) {
    if (scan_code == LSHIFT || scan_code == RSHIFT) {
      shifted = 1;
    }
    else if (char_map[scan_code] == 'p') {
	switch_to_idle();
    }
    else if (char_map[scan_code] == 'i') {
	switch_to_init();
    }
    else if (char_map[scan_code] == 'g') {
    }
    else {
      if (shifted)
	c = mayus_char_map[scan_code];
      else
	c = char_map[scan_code];
      printk_color(&c, YELLOW, BLACK);
    }
    print_cursor();
    //char n[8]; itoa(scan_code, n); printk_color(n, RED, BLACK); printk_color(" ", RED, BLACK);
  }
  else {
    if (scan_code == LSHIFT || scan_code == RSHIFT) {
      shifted = 0;
    }
  }
  return 0;
}

int clock_routine() {
  ++zeos_ticks;
  zeos_show_clock();
  schedule();
  return 0;
}
