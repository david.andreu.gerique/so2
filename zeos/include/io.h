/*
 * io.h - Definició de l'entrada/sortida per pantalla en mode sistema
 */

#ifndef __IO_H__
#define __IO_H__

#include <types.h>

/** Screen functions **/
/**********************/

// FG & BG
#define BLACK         0
#define BLUE          1
#define GREEN         2
#define CYAN          3
#define RED           4
#define MAGENTA       5
#define BROWN         6
#define LIGHT_GRAY    7
// Only FG
#define DARK_GRAY     8
#define LIGHT_BLUE    9
#define LIGHT_GREEN   10
#define LIGHT_CYAN    11
#define LIGHT_RED     12
#define LIGHT_MAGENTA 13
#define YELLOW        14
#define WHITE         15

#define BACKSPACE     8
#define LSHIFT        42
#define RSHIFT        54
#define BLK_MAYUS     29

extern int zeos_ticks;

Byte inb (unsigned short port);
void printc(char c);
void printc_xy(Byte x, Byte y, char c);
void printk(char *string);
void printc_color(char c, Byte fg, Byte bg, Byte blink);
void printk_color(char *string, Byte fg, Byte bg);
void printk_color_blink(char *string, Byte fg, Byte bg, Byte blink);
void clear_screen();

#endif  /* __IO_H__ */
