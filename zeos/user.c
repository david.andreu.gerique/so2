#include <libc.h>
#include <io.h>

char buff[24];

int pid;

int addASM(int par1, int par2);

void print(char* str) {
  if (write(1, str, strlen(str)) < 0) perror();
}

int __attribute__ ((__section__(".text.main")))
main(void)
{
  /* Next line, tries to move value 0 to CR3 register. This register is a privileged one, and so it will raise an exception */
  /* __asm__ __volatile__ ("mov %0, %%cr3"::"r" (0) ); */

/*  int t = 2250000;
  while(--t);
  char buffer[64] = "El temps fins ara es: ";
  if (write(1, buffer, strlen(buffer)) < 0) perror();
  itoa(gettime(), buffer);
  if (write(1, buffer, strlen(buffer)) < 0) perror();
  if (write(1, "\n", 1) < 0) perror();
  
  char buffer1[64] = "El PID d'aquest proces es: ";
  if (write(1, buffer1, strlen(buffer1)) < 0) perror();
  itoa(getpid(), buffer1);
  if (write(1, buffer1, strlen(buffer1)) < 0) perror();
  if (write(1, "\n", 1) < 0) perror();
*/
  char buffer[64] = "OJO que fem un fork\n";
  if (write(1, buffer, strlen(buffer)) < 0) perror();
  char bpid[10];
  for (int i = 0; i < 4; i++) {
    int ret = fork();
    if (ret == 0) {
      print("Child execution, PID: ");
      itoa(getpid(), bpid);
      print(bpid);
      print("\n");
    } else if (ret < 0) {
      perror();
    } else {
      print("Parent execution, PID: ");
      itoa(getpid(), bpid);
      print(bpid);
      print(", Child PID: ");
      itoa(ret, bpid);
      print(bpid);
      print("\n");
    }
  }

  int el_retard = 1e8;
  while(el_retard--);

  struct stats s;
  get_stats(getpid(), &s);

  print("User time: ");
  itoa(s.user_ticks, bpid);
  print(bpid);
  print("\n");

  print("System time: ");
  itoa(s.system_ticks, bpid);
  print(bpid);
  print("\n");

  print("Ready time: ");
  itoa(s.ready_ticks, bpid);
  print(bpid);
  print("\n");




  while(1) {
  }
}
