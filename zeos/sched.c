/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>

union task_union task[NR_TASKS]
__attribute__((__section__(".data.task")));

struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}

extern struct list_head blocked;

extern struct task_struct * idle_task;

extern struct task_struct * initial_task;

struct list_head freequeue;

struct list_head readyqueue;

struct task_struct * idle_task;

struct task_struct * initial_task;

#define DEFAULT_QUANTUM 2000

int quantum;

int proc_num = 2;

/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t) 
{
  return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t) 
{
  return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}


int allocate_DIR(struct task_struct *t) 
{
  int pos;

  pos = ((int)t-(int)task)/sizeof(union task_union);

  t->dir_pages_baseAddr = (page_table_entry*) &dir_pages[pos]; 

  return 1;
}

void cpu_idle(void)
{
  __asm__ __volatile__("sti": : :"memory");

  printk_color("Estem al IDLE (editor de python)\n", BLUE, BLACK);
  while(1)
    {
    }
}

void init_idle (void)
{
  struct list_head *t_anchor = list_first(&freequeue);
  list_del(t_anchor);	
  struct task_struct *task = list_head_to_task_struct(t_anchor);

  task->PID = 0;
  set_quantum(task, 1);
  allocate_DIR(task);

  int * stack = (int *)task;
  stack[1023] = cpu_idle;
  stack[1022] = 0xCAFE;
  task->KERNEL_ESP = &stack[1022];
	
  idle_task = task;
}

void init_task1(void)
{
  struct list_head *t_anchor = list_first(&freequeue);
  list_del(t_anchor);	
  struct task_struct *task = list_head_to_task_struct(t_anchor);

  task->PID = 1;
  set_quantum(task, DEFAULT_QUANTUM);
  quantum = get_quantum(task);
  allocate_DIR(task);
  set_user_pages(task);

  task->process_stats.user_ticks = 0;
  task->process_stats.system_ticks = 0;
  task->process_stats.blocked_ticks = 0;
  task->process_stats.ready_ticks = 0;
  task->process_stats.elapsed_total_ticks = get_ticks();
  task->process_stats.total_trans = 0;
  task->process_stats.remaining_ticks = get_quantum(task);

  int * stack = (int *)task;
  tss.esp0 = &stack[1024];
  writeMSR((long long)&stack[1024], 0x175);
  set_cr3(task->dir_pages_baseAddr);

  initial_task = task;
}


void init_sched()
{
  INIT_LIST_HEAD(&freequeue);
  INIT_LIST_HEAD(&readyqueue);

  for (int i = 0; i < NR_TASKS; i++) {
    list_add(&(task[i].task.list), &freequeue);
  } 
}

struct task_struct* current()
{
  int ret_value;
  
  __asm__ __volatile__(
		       "movl %%esp, %0"
		       : "=g" (ret_value)
		       );
  return (struct task_struct*)(ret_value&0xfffff000);
}

void task_switch(union task_union*t) {
  int * stack = (int *)t;
  tss.esp0 = &stack[1024];
  writeMSR((long long)&stack[1024], 0x175);
  set_cr3(t->task.dir_pages_baseAddr);
  inner_task_switch(t);
  // Update stats
  stats_ready_to_system();
  return;
}

void switch_to_init() {
  task_switch(initial_task);
}

void switch_to_idle() {
  task_switch(idle_task);
}

void sched_next_rr() {
  if (!list_empty(&readyqueue)) {
    struct task_struct *t = list_head_to_task_struct(list_first(&readyqueue));
    update_process_state_rr(t, NULL);
    quantum = get_quantum(t);
    t->process_stats.remaining_ticks = get_quantum(t);
    // t->quantum = t->initial_quantum;
    task_switch(t);
  } else switch_to_idle();
}

void update_process_state_rr(struct task_struct *t, struct list_head *dest) {
  //Only the processes that are not running have to be removed from their current list
  if (t != current()) {
    list_del(&(t->list));
  }
  //If the new state is running, the process is not added to any list
  if (dest != NULL) {
    list_add_tail(&(t->list), dest);
  }
}

int needs_sched_rr() {
  //Needs scheduling when the current process' quantum reaches 0
  return !quantum;
}

void update_sched_data_rr() {
  quantum--;
  current()->process_stats.remaining_ticks -= 1;
}

//Executed each clock tick
void schedule() {
  //Update current process' quantum
  update_sched_data_rr();

  //Schedule new process if necessary
  if (needs_sched_rr()) {
    update_process_state_rr(current(), &readyqueue);
    stats_system_to_ready();
    sched_next_rr();
  }
}

int get_quantum(struct task_struct *t) {
  return t->initial_quantum;
}

void set_quantum(struct task_struct *t, int quantum) {
  t->initial_quantum = quantum;
}

void stats_user_to_system() {
  current()->process_stats.user_ticks += get_ticks()-current()->process_stats.elapsed_total_ticks;
  current()->process_stats.elapsed_total_ticks = get_ticks();
}

void stats_system_to_user() {
  current()->process_stats.system_ticks += get_ticks()-current()->process_stats.elapsed_total_ticks;
  current()->process_stats.elapsed_total_ticks = get_ticks();
}

void stats_system_to_ready() {
  current()->process_stats.system_ticks += get_ticks()-current()->process_stats.elapsed_total_ticks;
  current()->process_stats.elapsed_total_ticks = get_ticks();
}

void stats_ready_to_system() {
  current()->process_stats.ready_ticks += get_ticks()-current()->process_stats.elapsed_total_ticks;
  current()->process_stats.elapsed_total_ticks = get_ticks();
}
