/*
 * io.c - 
 */

#include <io.h>

#include <types.h>

/**************/
/** Screen  ***/
/**************/

#define NUM_COLUMNS   80
#define NUM_ROWS      25

Byte x, y=19;

/* Read a byte from 'port' */
Byte inb (unsigned short port)
{
  Byte v;

  __asm__ __volatile__ ("inb %w1,%0":"=a" (v):"Nd" (port));
  return v;
}

void scroll_up() {
  Word *screen = (Word *)0xb8000;
  for (int i = 0; i < 80*25; ++i)
    screen[i] = (i > 80*24 ? (Word) 0x0F20 : screen[i+80]);
}

void clear_screen() {
  x = 0;
  y = 0;
  Word *screen = (Word *)0xb8000;
  for (int i = 0; i < 80*25; ++i)
    screen[i] = (Word) 0x0F20; // Space char
}

void printc(char c)
{
  __asm__ __volatile__ ( "movb %0, %%al; outb $0xe9" ::"a"(c)); /* Magic BOCHS debug: writes 'c' to port 0xe9 */
  if (c=='\n')
    {
      x = 0;
      y=(y+1)%NUM_ROWS;
    }
  else
    {
      Word ch = (Word) (c & 0x00FF) | 0x0200;
      Word *screen = (Word *)0xb8000;
      screen[(y * NUM_COLUMNS + x)] = ch;
      if (++x >= NUM_COLUMNS)
	{
	  x = 0;
	  y=(y+1)%NUM_ROWS;
	}
    }
}

void print_cursor() {
  char c = '_';
  Word color = (BLACK << 12) | ((0x0F & WHITE) << 8) | 0x8000;
  Word ch = (Word) (c & 0x00FF) | color;
  Word *screen = (Word *)0xb8000;
  screen[(y * NUM_COLUMNS + x)] = ch;
}

void printc_color(char c, Byte fg, Byte bg, Byte blink) {

  __asm__ __volatile__ ( "movb %0, %%al; outb $0xe9" ::"a"(c)); /* Magic BOCHS debug: writes 'c' to port 0xe9 */
  if (c=='\n')
    {
      Word ch = (Word) (c & 0x00FF) | 0x0200;
      Word *screen = (Word *)0xb8000;
      screen[(y * NUM_COLUMNS + x)] = ' ';

      x = 0;
      if (y < NUM_ROWS - 1)
	y = y + 1;
      else
	scroll_up();
    }
  else if (c == BACKSPACE) {
    Word ch = (Word) (c & 0x00FF) | 0x0200;
    Word *screen = (Word *)0xb8000;
    screen[(y * NUM_COLUMNS + x)] = ' ';
    x = (x > 0 ? x - 1 : x);
    screen[(y * NUM_COLUMNS + x)] = ' ';
  }
  else
    {
      Word color = ((bg > LIGHT_GRAY ? BLACK : bg) << 12) |
	((0x0F & fg) << 8) |
	(blink > 0 ? 0x8000 : 0x0);
      Word ch = (Word) (c & 0x00FF) | color;
      Word *screen = (Word *)0xb8000;
      screen[(y * NUM_COLUMNS + x)] = ch;
      if (++x >= NUM_COLUMNS)
	{
	  x = 0;
	  if (y < NUM_ROWS - 1)
	    y = y + 1;
	  else
	    scroll_up();
	}
    }
}


void printc_xy(Byte mx, Byte my, char c)
{
  Byte cx, cy;
  cx=x;
  cy=y;
  x=mx;
  y=my;
  printc(c);
  x=cx;
  y=cy;
}

void printk(char *string)
{
  int i;
  for (i = 0; string[i]; i++)
    printc(string[i]);
}

void printk_color(char *string, Byte fg, Byte bg) {
  printk_color_blink(string, fg, bg, 0);
}

void printk_color_blink(char *string, Byte fg, Byte bg, Byte blink) {
  int i;
  for (i = 0; string[i]; i++)
    printc_color(string[i], fg, bg, blink);
}


